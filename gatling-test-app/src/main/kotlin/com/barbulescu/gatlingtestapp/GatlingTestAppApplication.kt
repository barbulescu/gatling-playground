package com.barbulescu.gatlingtestapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicInteger

@SpringBootApplication
class GatlingTestAppApplication

fun main(args: Array<String>) {
    runApplication<GatlingTestAppApplication>(*args)
}

@RestController
class HelloController {
    private val counter: AtomicInteger = AtomicInteger(0)

    @GetMapping("/hello/{name}")
    fun sayHello(@PathVariable name: String): String {
        val index = counter.getAndIncrement()
        println("start $index on thread ${Thread.currentThread().name}" )
        Thread.sleep(1000)
        println("stop $index")
        return "Hello $name"
    }
}