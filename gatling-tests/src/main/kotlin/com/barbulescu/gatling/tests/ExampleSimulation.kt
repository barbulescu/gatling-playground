package com.barbulescu.gatling.tests

import com.barbulescu.gatling.tests.PerfTestConfig.DURATION_MIN
import com.barbulescu.gatling.tests.PerfTestConfig.P95_RESPONSE_TIME_MS
import com.barbulescu.gatling.tests.PerfTestConfig.REQUEST_PER_SECOND
import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.core.OpenInjectionStep
import io.gatling.javaapi.core.Simulation
import io.gatling.javaapi.http.HttpDsl.http
import io.gatling.javaapi.http.HttpDsl.status
import java.time.Duration


class ExampleSimulation : Simulation() {

    private val httpProtocol = http.baseUrl(PerfTestConfig.BASE_URL)
        .header("Content-Type", "text/plain")
        .header("Accept-Encoding", "gzip")
        .check(status().`is`(200))

    private val scn = scenario("Hello end point calls")
        .exec(http("hello end point").get("/hello/Marius"))

    init {
//        val users: OpenInjectionStep = rampUsers(20).during(Duration.ofSeconds(60))
        val users = incrementConcurrentUsers(5)
            .times(100)
            .eachLevelLasting(Duration.ofSeconds(20))
        setUp(scn.injectClosed(users))
            .protocols(httpProtocol)
            .assertions(global().responseTime().percentile3().lt(P95_RESPONSE_TIME_MS),
                global().successfulRequests().percent().gt(95.0))
    }
}